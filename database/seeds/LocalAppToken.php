<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class LocalAppToken extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\LocalAppRegistered::create([
            'name' => "Example App",
            'status' => true,
            'remember_token' => str_random(100),
        ]);
    }
}
