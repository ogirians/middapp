<?php

namespace App\Http\Middleware;

use Closure;
use App\LocalAppRegistered;

class AuthenticatedForLocalApp
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($request->bearerToken()) {
            $auth = LocalAppRegistered::where("remember_token", $request->bearerToken())
            ->where("status", true);
            if($auth->count() > 0) {
                return $next($request);
            } else {
                return response([
                    'message' => 'Unauthenticated'
                ], 401);
            }
        }
        return response([
            'message' => 'Forbidden'
        ], 403);
    }
}
