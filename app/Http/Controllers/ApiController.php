<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;


class ApiController extends Controller
{
    public function CekStatus($nik) {
    	$client = new Client();
		$res = $client->get('http://esign.jatimprov.go.id/api/user/status/'.$nik, ['auth' =>  ['soetomo_pengadaan', 'soetomo123!@#']]);
		//echo $res->getStatusCode(); // 200
		$response = $res->getBody();

        return $response;
    }
}
